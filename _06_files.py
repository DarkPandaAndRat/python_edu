FILE:

open() function opens and returns a file handle 

code f = open('name', 'r')		#opens the file into the variable f
f.close()				#close file

code f = open('name', 'r')		#r - read
code f = open('name', 'w')		#w - write
code f = open('name', 'a')		#a - append


# Echo the contents of a text file
f = open('foo.txt', 'rt', encoding='utf-8')
for line in f:			## iterates over the lines of the file
    print(line, end='')		## end='' so print does not add an end-of-line char
				## since 'line' already includes the end-of-line.
f.close()

"r" - Read - Default value. Opens a file for reading, error if the file does not exist
"a" - Append - Opens a file for appending, creates the file if it does not exist
"w" - Write - Opens a file for writing, creates the file if it does not exist
"x" - Create - Creates the specified file, returns an error if the file exists
"t" - Text - Default value. Text mode. To read and write unicode encoded files use a `'t'` mode and explicitly specify an encoding:
"b" - Binary - Binary mode (e.g. images)


FILE UNICODE:

with open('foo.txt', 'rt', encoding='utf-8') as f:
  for line in f:
    # here line is a *unicode* string

with open('write_test', encoding='utf-8', mode='wt') as f:
    f.write('\u20ACunicode\u20AC\n') #  €unicode€
    # AKA print('\u20ACunicode\u20AC', file=f)  ## which auto-adds end='\n'


