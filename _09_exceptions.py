#!/usr/bin/env python3

#Exceptions
try:
    ## Either of these two lines could throw an IOError, say
    ## if the file does not exist or the read() encounters a low level error.
    filename = "/home/file.txt"
    f = open(filename, 'rb')
    data = f.read()
    f.close()
except IOError:
    ## Control jumps directly to here if any of the above lines throws IOError.
    sys.stderr.write('problem reading:' + filename)
    ## In any case, the code then continues with the line after the try/except

