#Dict Hash Table - key/value hash table structure a "dict"
dict = {key1:value1, key2:value2, ... }
dict = {}

  ## Can build up a dict by starting with the the empty dict {}
  ## and storing key/value pairs into the dict like this:
  ## dict[key] = value-for-that-key
  dict = {}
  dict['a'] = 'alpha'
  dict['g'] = 'gamma'
  dict['o'] = 'omega'

  print(dict) ## {'a': 'alpha', 'o': 'omega', 'g': 'gamma'}

  print(dict['a'])			## Simple lookup, returns 'alpha'
  dict['a'] = 6				## Put new key/value into dict
  'a' in dict         			## True
  ## print(dict['z'])			## Throws KeyError
  if 'z' in dict: print(dict['z'])	## Avoid KeyError
  print(dict.get('z'))			## None (instead of KeyError)
  for key in dict: print(key)		## prints a g o
  for key in dict.keys(): print(key)	## Exactly the same as above

  # Get the .keys() list:
  print(dict.keys())  ## dict_keys(['a', 'o', 'g'])

  # Likewise, there's a .values() list of values
  print(dict.values())  ## dict_values(['alpha', 'omega', 'gamma'])

  # Common case -- loop over the keys in sorted order,
  # accessing each key/value
  for key in sorted(dict.keys()):
    print(key, dict[key])

  # .items() is the dict expressed as (key, value) tuples
  print(dict.items())  ##  dict_items([('a', 'alpha'), ('o', 'omega'), ('g', 'gamma')])

  ## This loop syntax accesses the whole dict by looping
  ## over the .items() tuple list, accessing one (key, value)
  ## pair on each iteration.
  for k, v in dict.items(): print(k, '>', v)
  ## a > alpha    o > omega     g > gamma


