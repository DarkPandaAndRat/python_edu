#!/usr/bin/env python

#sort method must be called on a list (only on existing list)
list.sort()
alist = list.sort()	#INCORRCT - RETURN NOTHING


#sorting is a different method
a = [5, 1, 4, 3]
print(sorted(a))
print(a)

strs = ['aa', 'BB', 'zz', 'CC']
print(sorted(strs))
print(sorted(strs, reverse=True))

#sorting with key
strs = ['ccc', 'aaaa', 'd', 'bb']
print(sorted(strs, key=len))

#"key" argument specifying str.lower function to use for sorting
print(sorted(strs, key=str.lower))

#You can also pass in your own MyFn as the key function, like this:
#Say we have a list of strings we want to sort by the last letter of the string.
strs = ['xc', 'zb', 'yd' ,'wa']

# Write a little function that takes a string, and returns its last letter.
# This will be the key function (takes in 1 value, returns 1 value).
def MyFn(s):
    return s[-1]

# Now pass key=MyFn to sorted() to sort by the last letter:
print(sorted(strs, key=MyFn))


from operator import itemgetter
# (first name, last name, score) tuples
grade = [('Freddy', 'Frank', 3), ('Anil', 'Frank', 100), ('Anil', 'Wang', 24)]
print(sorted(grade, key=itemgetter(1,0)))
print(sorted(grade, key=itemgetter(0,-1)))

## Select values <= 2
nums = [2, 8, 1, 6]
small = [ n for n in nums if n <= 2 ]  ## [2, 1]

## Select fruits containing 'a', change to upper case
fruits = ['apple', 'cherry', 'banana', 'lemon']
afruits = [ s.upper() for s in fruits if 'a' in s ]







